import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import config from "./ormConfig";
import {UserModule} from "./user/user.module";
import {GroupModule} from "./group/group.module";

@Module({
  imports: [TypeOrmModule.forRoot(config),UserModule,GroupModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
