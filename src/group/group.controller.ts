import {Body, Controller, Delete, Get, Param, Post, Put, UsePipes, ValidationPipe} from "@nestjs/common";
import {GroupService} from "./group.service";
import {CreateUserDto} from "../user/dto/createUser.dto";
import {UpdateUserDto} from "../user/dto/updateUser.dto";
import {CreateGroupDto} from "./dto/createGroup.dto";
import {UpdateGroupDto} from "./dto/updateGroup.dto";


@Controller('groups')
export class GroupController {
    constructor(private readonly groupService: GroupService) {
    }

    @Get()
    getAll()  {
        return this.groupService.getAll()
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.groupService.findByID(id)
    }

    @Post()
    @UsePipes(new ValidationPipe())
    createGroup(@Body() CreateGroupDto: CreateGroupDto) {
        return this.groupService.createGroup(CreateGroupDto)

    }

    @Delete(':id')
    remove(@Param('id') id:number){
        return this.groupService.remove(id)
    }

    @Put(':id')
    @UsePipes(new ValidationPipe())
    updateGroup(@Body() UpdateGroupDto:UpdateGroupDto, @Param('id') id:number)
    {
        return this.groupService.updateGroup(id,UpdateGroupDto)
    }
}