import {  IsNotEmpty } from 'class-validator';

export class UpdateGroupDto{
    @IsNotEmpty()
    readonly groupName:string

}