import {  IsNotEmpty } from 'class-validator';


export class CreateGroupDto{
    @IsNotEmpty()
    readonly groupName:string

}