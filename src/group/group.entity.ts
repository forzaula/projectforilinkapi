import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {UserEntity} from "../user/user.entity";

@Entity({name:'groups'})
export class GroupEntity{
    @PrimaryGeneratedColumn()
    id:number

    @Column()
    groupName:string

    @ManyToMany(()=>UserEntity,user=>user.groups)
    users:UserEntity[]
}