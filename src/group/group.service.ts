import {HttpException, HttpStatus, Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {DeleteResult, Repository} from "typeorm";
import {GroupEntity} from "./group.entity";
import {CreateGroupDto} from "./dto/createGroup.dto";
import {UpdateGroupDto} from "./dto/updateGroup.dto";



@Injectable()
export class GroupService {
    constructor(
        @InjectRepository(GroupEntity)
        private readonly groupRepository: Repository<GroupEntity>) {
    }


    async createGroup(CreateGroupDto: CreateGroupDto): Promise<GroupEntity> {
        const groupName = await this.groupRepository.findOne({groupName: CreateGroupDto.groupName})
        if (groupName) {
            throw new HttpException("This name is taken", HttpStatus.UNPROCESSABLE_ENTITY)
        }
        const newGroup = new GroupEntity()
        Object.assign(newGroup, CreateGroupDto)
        return await this.groupRepository.save(newGroup)
    }
    async getAll(){
        return await this.groupRepository.find()
    }
    async findByID(id:number):Promise<GroupEntity>{
        return await this.groupRepository.findOne(id)
    }
    async updateGroup(id:number,UpdateGroupDto:UpdateGroupDto){
        const group= await this.findByID(id)
        if(!group){
            throw new HttpException('group does not exist',HttpStatus.NOT_FOUND)
        }
        Object.assign(group,UpdateGroupDto)
        return await this.groupRepository.save(group)
    }
    async remove(id:number):Promise<DeleteResult>{
        const group= await this.findByID(id)
        if(!group){
            throw new HttpException('group does not exist',HttpStatus.NOT_FOUND)
        }
        return this.groupRepository.delete({id})
    }
}