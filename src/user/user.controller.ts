import {Body, Controller, Delete, Get, Param, Post, Put, UsePipes, ValidationPipe} from "@nestjs/common";
import {CreateUserDto} from "./dto/createUser.dto";
import {UpdateUserDto} from "./dto/updateUser.dto";
import {UserService} from "./user.service";


@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {
    }

    @Get()
    getAll()  {
        return this.userService.getAll()
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.userService.findByID(id)
    }

    @Post()
    @UsePipes(new ValidationPipe())
    createUser(@Body() createUserDto: CreateUserDto) {
        return this.userService.createUser(createUserDto)

    }

    @Delete(':id')
    remove(@Param('id') id:number){
        return this.userService.remove(id)
    }

    @Put(':id')
    @UsePipes(new ValidationPipe())
    updateUser(@Body() updateUserDto:UpdateUserDto, @Param('id') id:number)
    {
        return this.userService.updateUser(id,updateUserDto)
    }
}
