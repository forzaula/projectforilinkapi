import {BeforeInsert, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {GroupEntity} from "../group/group.entity";
import {hash}from 'bcrypt'


@Entity({name:'users'})
export class UserEntity{
    @PrimaryGeneratedColumn()
    id:number

    @Column()
    username:string

    @Column()
    email:string

    @Column()
    password:string

    @BeforeInsert()
    async hashPassword(){
        this.password= await hash(this.password,10)
    }

    @Column('simple-array',{nullable: true})
    friends:string[]

    @ManyToMany(()=>GroupEntity,group=>group.users,{eager:true})
    @JoinTable()
    groups:GroupEntity[]
}